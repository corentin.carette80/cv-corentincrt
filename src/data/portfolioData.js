export const portfolioData = [
  {
    id: 1,
    name: "CV Corentin Carette",
    languages: ["javascript", "css", "react"],
    languagesIcons: ["fab fa-js", "fab fa-css3-alt", "fab fa-react"],
    source: "https://gitlab.com/corentin.carette80/cv-corentincrt",
    info: "CV que vous êtes actuellement entrain de regarder",
    picture: "./media/project1.JPG",
  },
  {
    id: 2,
    name: "API Film (moviedb)",
    languages: ["javascript", "react", "css"],
    languagesIcons: ["fab fa-js", "fab fa-css3-alt", "fab fa-react"],
    source: "https://gitlab.com/corentin.carette80/api-react-film",
    info: "Utilisation de l'api moviedb pour rechercher un film et l'ajouter à ses favoris en React",
    picture: "./media/project2.JPG",
  },
  //"php" =>"fab fa-php"
  //"node" =>"fab fa-node"
];
